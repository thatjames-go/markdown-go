DIST_DIR := $(PWD)/dist
BIN_DIR := ${DIST_DIR}/bin

all: compile

docker:
	@docker-compose build

golint: godep
	@golint ./...

gotest:
	@go test -short ./...

gocoverage:
	@./coverage.sh;

gocoverhtml:
	@./coverage.sh -h

godep:
	@go get golang.org/x/lint/golint
	@go install golang.org/x/lint/golint
	@go get -v -d ./...

compile: godep
	go build -v

build: godep
	@echo "Make amd64_linux"
	@env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -o ${BIN_DIR}/markdown-go

build_all: godep amd64 arm64 win64
