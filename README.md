[![pipeline status](https://gitlab.com/ThatJames/markdown-go/badges/development/pipeline.svg)](https://gitlab.com/ThatJames/markdown-go/-/commits/development)
# Markdown Go
## Simple golang markdown to html

Uses GitHub's markdown API to convert markdown into HTML

## Installation
### From Source
Requires a working installation of [golang](https://golang.org/doc/install) and for `$GOPATH/bin` to be declared in your `$PATH`

```
go get gitlab.com/ThatJames/markdown-go
go install gitlab.com/ThatJames/markdown-go
```

### Download Binaries
Check out the [releases](https://gitlab.com/ThatJames/markdown-go/-/releases) page for all available releases


## Vim Integration
Can be integrated into vim or neovim with a simple hook, like below
```Lua
function MarkdownGo()
    if !system('pgrep markdown-go')
        silent !markdown-go -t <your_github_token> % &> /dev/null &
    endif
    !firefox localhost:8008/page
endfunction

augroup autocom
    autocmd!
    autocmd VimLeave * !killall markdown-go
augroup end

let mapleader=","
map <leader>p :silent call MarkdownGo()<CR>
```
