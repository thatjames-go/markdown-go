package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"text/template"
	"time"

	"git.slimjim.xyz/slimjim/markdown-go/domain"
	"git.slimjim.xyz/slimjim/markdown-go/render"
	"github.com/fsnotify/fsnotify"
	"github.com/gorilla/mux"
)

const (
	githubAPI = "https://api.github.com/markdown"
)

var (
	readMe          []byte
	title           string
	longPollRelease chan bool = make(chan bool)

	token     string
	addr      string
	renderMap map[string]render.FileRenderer
)

func usage() {
	fmt.Println("usage: markdown-go <options> FILE")
	fmt.Println("options:")
	flag.PrintDefaults()
}

func main() {
	flag.StringVar(&token, "t", "", "github token")
	flag.StringVar(&addr, "l", "127.0.0.1:8008", "listen address")
	flag.Usage = usage
	flag.Parse()
	if len(flag.Args()) < 1 {
		flag.Usage()
		os.Exit(0)
	}

	renderMap := make(map[string]render.FileRenderer)
	if token != "" {
		renderMap["md"] = render.NewGitHubMarkdownRender(token)
	}

	router := mux.NewRouter()
	var err error
	if readMe, err = ioutil.ReadFile(flag.Arg(0)); err != nil {
		panic(err)
	}
	go watchFile(flag.Arg(0))
	filepath.Walk(".", func(path string, info os.FileInfo, errIn error) (err error) {
		if errIn != nil {
			return errIn
		}
		if strings.HasSuffix(info.Name(), ".md") {
			fmt.Println("Loading file", info.Name())
			title = info.Name()
			if readMe, err = ioutil.ReadFile(info.Name()); err == nil {
				go watchFile(info.Name())
			}
		} else if info.IsDir() && info.Name() != "." {
			name := fmt.Sprintf("/%s/", info.Name())
			router.PathPrefix(name).Handler(http.StripPrefix(name, http.FileServer(http.Dir(info.Name()))))
		}
		return
	})
	router.HandleFunc("/css/github.css", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/css")
		w.WriteHeader(http.StatusOK)
		w.Write(domain.CssPage)
	})
	router.HandleFunc("/longpoll/", handleLongPoll)
	router.HandleFunc("/page", handlePage)
	if err := http.ListenAndServe(addr, router); err != nil {
		panic(err)
	}
}

func handlePage(w http.ResponseWriter, r *http.Request) {
	requestBuffer := new(bytes.Buffer)
	json.NewEncoder(requestBuffer).Encode(&render.GitHubAPI{
		Text: string(readMe),
		Mode: "markdown",
	})

	req, err := http.NewRequest(http.MethodPost, githubAPI, requestBuffer)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if len(token) > 0 {
		req.Header.Set("Authorization", "token "+token)
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer resp.Body.Close()

	dat, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	content := &render.Render{
		Title:   title,
		Content: string(dat),
	}

	pageTemplate := template.Must(template.New("content").Parse(domain.HTMLPage))
	w.WriteHeader(http.StatusOK)
	if err := pageTemplate.Execute(w, content); err != nil {
		log.Println("[ERROR]", err.Error())
	}

}

func handleLongPoll(w http.ResponseWriter, r *http.Request) {
	select {
	case <-longPollRelease:
	case <-time.After(time.Minute):
		w.WriteHeader(http.StatusRequestTimeout)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func watchFile(f string) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		fmt.Println("[ERROR]", err.Error())
		return
	}

	defer watcher.Close()
	watcher.Add(path.Dir(f))
	for event := range watcher.Events {
		if event.Op&fsnotify.Write > 0 && strings.Contains(event.Name, f) {
			readMe, _ = ioutil.ReadFile(f)
			fmt.Println(event.Name, "updated")
			select {
			case longPollRelease <- true:
			default: //Don't signal the client twice
			}
		}
	}
}
