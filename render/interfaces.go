package render

import (
	"os"
)

type FileRenderer interface {
	Render(*os.File) (*Render, error)
}

type Render struct {
	Title   string
	Content string
}

type FileRenderType int

const (
	UnknownRender FileRenderType = iota
	Markdown
	HTML
)

func NewRender(renderType FileRenderType) FileRenderer {
	switch renderType {
	case Markdown:
		return nil
	case HTML:
		return nil
	case UnknownRender:
		return nil
	default:
		return nil
	}
}
