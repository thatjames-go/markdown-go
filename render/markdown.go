package render

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
)

const (
	githubAPI = "https://api.github.com/markdown"
)

type GitHubAPI struct {
	Text string `json:"text"`
	Mode string `json:"mode"`
}

type GitHubMarkdownRender func(*os.File) (*Render, error)

func (g GitHubMarkdownRender) Render(f *os.File) (*Render, error) {
	return g(f)
}

func NewGitHubMarkdownRender(token string) FileRenderer {
	return GitHubMarkdownRender(func(f *os.File) (*Render, error) {
		requestBuffer := new(bytes.Buffer)
		dat, err := ioutil.ReadAll(f)
		if err != nil {
			return nil, err
		}
		json.NewEncoder(requestBuffer).Encode(&GitHubAPI{
			Text: string(dat),
			Mode: "markdown",
		})

		req, err := http.NewRequest(http.MethodPost, githubAPI, requestBuffer)
		if err != nil {
			return nil, err
		}
		if len(token) > 0 {
			req.Header.Set("Authorization", "token "+token)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return nil, err
		}

		defer resp.Body.Close()

		dat, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		return &Render{
			Title:   f.Name(),
			Content: string(dat),
		}, nil
	})
}
