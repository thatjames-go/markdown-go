module git.slimjim.xyz/slimjim/markdown-go

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gorilla/mux v1.7.4
)
