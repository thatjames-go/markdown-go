# Markdown Go Issue


 Please be sure to follow these instructions as closely as possible to help me in helping you


## Version
Tag / Commit ID

## Platform
Windows / Linux / Mac / Other

## Short Description of the bug

## Expected behaviour vs actual behavior

## Any Additional info?
